---
categories: [pages]
date: {{ .Date }}
draft: true
menu: ""
slug: {{ .TranslationBaseName }}
tags: [tag1]
thumb: path-to-thumbnail-image-over-ssl
title: {{ replace .TranslationBaseName "-" " " | title }}
---
