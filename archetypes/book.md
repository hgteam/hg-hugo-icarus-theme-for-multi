---
date: {{ .Date }}
slug: {{ .TranslationBaseName }}
tags: [book]
thumb: path-to-thumbnail-image-over-ssl
title: 本のタイトル
---

<!-- amazonのリンクをここに入れる -->

## 概要
<!-- 著者についての情報や言及しているジャンルなど -->

## 主題
<!-- 著者が言いたがっていること -->

## 行動
<!-- 実行に移せそうな事柄 -->

## 関連情報
<!-- 他の本やメディアと一致する見解があるか -->

## 所感・特記事項

